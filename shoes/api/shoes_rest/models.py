from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.PositiveSmallIntegerField()
    closet_name = models.CharField(max_length=100)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    pic_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name} - {self.color}"

    def get_api_url(self):
        return reverse("api_show_bin", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("manufacturer","model_name","color")
