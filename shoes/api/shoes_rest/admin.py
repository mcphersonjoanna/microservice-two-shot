from django.contrib import admin

# Register your models here.
from .models import Shoe, BinVO

admin.site.register(Shoe)
admin.site.register(BinVO)
