# Generated by Django 4.0.3 on 2023-07-20 21:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='closet_name',
            field=models.CharField(default='closet', max_length=100),
            preserve_default=False,
        ),
    ]
