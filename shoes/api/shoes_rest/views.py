from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [ "import_href","bin_number","closet_name"]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "pic_url",
        "bin",
    ]
    encoders ={
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    """
    Collection RESTful API handler for Location objects in
    the wardrobe.

    GET:
    Returns a dictionary with a single key "shoes" which
    is a list of the manufacturer, model_name, color, pic_url,and id.

    POST:
    Creates a shoe and shows it's details
    """
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                shoe=ShoeEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            properties = ["manufacturer",
                            "model_name",
                            "color",
                            "pic_url",
                            "bin",
                        ]
            for prop in properties:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
