function ShoeList({shoes, getShoes}) {

  async function handleShoeDelete(shoe_id) {
    const shoeUrl =`http://localhost:8080/api/shoes/${shoe_id}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      console.log("deleted");
      getShoes();
    }
  }
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Shoe Manufacturer</th>
            <th>Shoe Model</th>
            <th>Shoe Color</th>
            <th>Shoe Bin Closet & Number</th>
            <th>Shoe Picture</th>
            <th>Trash!</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (

              <tr key={shoe.id}>
                <td >{ shoe.manufacturer}</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name}-{ shoe.bin.bin_number}</td>
                <td> <img src={shoe.pic_url} alt="shoe" style={{ width: '100px', height: '100px' }} /></td>
                <td>
                  <button onClick={() => handleShoeDelete(shoe.id)} className="btn btn-danger"> Trash This Shoe?</button>

                </td>
              </tr>


            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
