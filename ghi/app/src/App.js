
import HatForm from './HatForm';
import HatList from './HatList';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';

function App() {
  const [ bins, setBins ] = useState([]);
  const [ shoes, setShoes ] = useState([]);
  const [locations, setLocations] = useState([]);
  const [hats, setHats] = useState([]);

  async function getHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if(response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else (
      console.error('An error occured fetching the hat data')
    )
  }

  async function getLocations() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    } else {
      console.error('An error occured fetching the location data')
    }
  }
  useEffect(() => {
    getHats();
    getLocations();
  }, [])

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred fetching the shoe data')
    }
  }
  async function getBins() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const {bins} = await response.json();
      setBins(bins);
    }
    else {
      console.error('An error occurred fetching the bin data')
    }
  }

  useEffect(() => {
    getBins();
    getShoes();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={hats} getHats={getHats} />} />
            <Route path="new" element={<HatForm locations={locations} getHats={getHats} />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList shoes={shoes} getShoes={getShoes}/>} />
            <Route path="new" element={<ShoeForm bins={bins} getShoes={getShoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
  }


export default App;
