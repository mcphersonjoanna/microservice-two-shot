import React, {useEffect, useState} from 'react';

function HatForm ({getHats}) {
    const [locations, setLocations] = useState([]);
    const [name,setStyle] =useState('');
    const [fabric, setFabric] = useState('');
    const [hat_color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);

    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};

        data.style_name = name;
        data.fabric = fabric;
        data.color = hat_color;
        data.picture_url = picture;
        data.location = location;

        console.log(data);

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
          console.log(newHat);

          setFabric('');
          setStyle('');
          setColor('');
          setPicture('');
          setLocation('');
          getHats();
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>create a hat!</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleStyleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">style name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={hat_color} onChange={handleColorChange} placeholder="Color" required type="text" name="hat_color" id="hat_color" className="form-control"/>
                <label htmlFor="hat_color">color</label>
              </div>
              <div className="form-floating mb-3">
                <input value= {picture} onChange={handlePictureChange}  placeholder="Picture"  type="url" name="picture" id="picture" className="form-control"/>
                <label htmlFor="picture">picture url</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">choose a location</option>
                  {locations.map(location =>{
                    return (
                        <option key={location.id} value={location.href}>
                            {location.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default HatForm;
