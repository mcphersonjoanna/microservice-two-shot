import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Wardrobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            {/* SHOES DROPDOWN */}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" id="shoesMenu" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Shoes
              </a>
              <ul className="dropdown-menu" aria-labelledby="shoesMenu">
                <li><NavLink className="dropdown-item" to="/shoes">All shoes</NavLink></li>
                <div className="dropdown-divider"></div>
                <li><NavLink className="dropdown-item" to="/shoes/new">Create a shoe</NavLink></li>
              </ul>
            </li>
            {/* HATS DROPDOWN */}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" id="hatsMenu" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Hats
              </a>
              <ul className="dropdown-menu" aria-labelledby="hatsMenu">
                <li><NavLink className="dropdown-item" to="/hats">All hats</NavLink></li>
                <div className="dropdown-divider"></div>
                <li><NavLink className="dropdown-item" to="/hats/new">Create a hat</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
