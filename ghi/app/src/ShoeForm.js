import React, { useState } from 'react';

function ShoeForm({getShoes, bins}) {
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [pic_url, setPicUrl] = useState('');
  const [color, setColor] = useState('');
  const [bin, setBin] = useState('');

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      manufacturer: manufacturer,
      model_name: modelName,
      pic_url:pic_url,
      color:color,
      bin:bin,
    };

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newBin = await response.json();
      console.log(newBin);
      setManufacturer('');
      setModelName('');
      setPicUrl('');
      setColor('');
      setBin('');
      getShoes();
    }
  }

  function handleManufacturerChange(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleModelNameChange(event) {
    const { value } = event.target;
    setModelName(value);
  }

  function handlePicUrlChange(event) {
    const { value } = event.target;
    setPicUrl(value);
  }

  function handleColorChange(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handleBinChange(event) {
    const { value } = event.target;
    setBin(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} value={manufacturer} maxLength={100} placeholder="Manufacturer" required type="text" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleModelNameChange} value={modelName} maxLength={100} placeholder="Model name" type="text" id="model_name" className="form-control" required/>
              <label htmlFor="model_name">Model name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePicUrlChange} value={pic_url} placeholder="picture url" type="url" id="pic_url" className="form-control" />
              <label htmlFor="pic_url">Picture url</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} maxLength={100} id="color" className="form-control"></input>
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} value={bin.href} required className="form-select" id="bin">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name} - {bin.bin_number}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
