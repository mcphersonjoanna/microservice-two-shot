import React, {  useState } from 'react';

function HatList({hats, getHats}) {
    async function handleHatDelete(hat_id) {
        const hatUrl = `http://localhost:8090/api/hats/${hat_id}`;
        const fetchConfig = {
            method:"delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            console.log("hat deleted");
            getHats();
        }
    }
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>hat style</th>
            <th>hat color</th>
            <th>hat fabric</th>
            <th>hat location</th>
            <th>hat pic</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.location }</td>
                <td><img src={ hat.picture_url } alt="hat" style={{ width: '100px', height:'100px' }}/></td>
                <td><button onClick={() => handleHatDelete(hat.id)} className="btn btn-danger">Delete hat?</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;
