from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Hat
from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture_url",
        "id",
    ]

    def get_extra_data(self, o):
        return {"location":o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture_url",
        "id",
        "location",
    ]

    encoders = {
        "location": LocationVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def show_hat_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location=LocationVO.objects.get(import_href=location_href)
            content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "location id is invalid"},
                status=400,
            )

        hat=Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE","GET"])
def delete_hats(request,id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
                  )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse({"message": "Hat deleted"})
        except Hat.DoesNotExist:
            return JsonResponse({"message": "does not exist"})
