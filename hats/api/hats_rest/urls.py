from django.urls import path
from .views import show_hat_list, delete_hats
urlpatterns = [
    path("hats/", show_hat_list, name="api_hats"),
    path("hats/<int:id>/", delete_hats,name="delete_hats" )
]
